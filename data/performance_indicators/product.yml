- name: Total Monthly Active Users (TMAU)
  base_path: "/handbook/product/performance-indicators/"
  definition: The sum of all stage monthly active users (<a href="https://about.gitlab.com/handbook/product/performance-indicators/#stage-monthly-active-users-smau">SMAU</a>)
    in a rolling 28 day period. Numbers displayed below for self-managed are for instances
    with usage ping (<a href="https://app.periscopedata.com/app/gitlab/634200/Usage-Ping-SMAU-Dashboard?widget=8462059&udv=0">enabled</a>).
    Owner of this KPI is the VP, Product Management.
  target: 
  org: Product
  public: true
  is_key: true
  health:
    level: 0
    reasons:
    - TBD
  sisense_data:
    chart: 9051075
    dashboard: 634200
    embed: v2
- name: Paid Total Monthly Active Users (Paid TMAU)
  base_path: "/handbook/product/performance-indicators/"
  definition: The sum of all paid stage monthly active users in a 28 day rolling period.
    The license information used to generate this metric is being validated, and the
    currently displayed data could be incorrect by up to 20%.  Numbers displayed below
    for self-managed are for instances with usage ping (<a href="https://app.periscopedata.com/app/gitlab/634200/Usage-Ping-SMAU-Dashboard?widget=8462059&udv=0">enabled</a>)
    Owner of this KPI is the VP, Product Management.
  target: 
  org: Product
  public: true
  is_key: true
  health:
    level: 0
    reasons:
    - See notes in Key Meeting slides
  sisense_data:
    chart: 9051099
    dashboard: 634200
    embed: v2
- name: Monthly Active Users (MAU)
  base_path: "/handbook/product/performance-indicators/"
  definition: The number of unique users that performed an <a href="https://docs.gitlab.com/ee/api/events.html">event</a>
    within the previous 28 days. Owner of this KPI is the VP, Product Management.
  target: 
  org: Product
  public: true
  is_key: true
  health:
    level: 2
    reasons:
    - See notes in Key Meeting slides
  sisense_data:
    chart: 8079819
    dashboard: 527913
    embed: v2
- name: Direct signup ARR growth rate
  base_path: "/handbook/product/performance-indicators/"
  definition: Direct signup ARR growth rate is calculated by measuring the difference
    in actual signup ARR between two consecutive periods (current and prior) and dividing
    that number by the direct signup ARR of the prior period. Owner of this KPI is
    the <a href="https://about.gitlab.com/direction/acquisition/">Growth - Acquisition</a>
    group. The calculation for this metric is (Direct signup ARR in the current period
    - direct signup revenue in the prior period)/direct signup revenue in the prior
    period.
  target: 
  org: Product
  public: false
  is_key: true
  health:
    level: 2
    reasons:
    - See notes in Key Meeting slides
  urls:
  - https://app.periscopedata.com/app/gitlab/527913/Product-KPIs?widget=7634169&udv=0
- name: Free to paid ARR growth rate
  base_path: "/handbook/product/performance-indicators/"
  definition: Free to Paid ARR growth rate is calculated by measuring the difference
    in actual free to paid ARR between two consecutive periods (current and prior)
    and dividing that number by the free to paid ARR of the prior period. Owner of
    this KPI is the <a href="https://about.gitlab.com/direction/conversion/">Growth
    - Conversion</a> group. The calculation for this metric is (Free to paid signup
    ARR in the current period - free to paid signup revenue in the prior period) /
    free to paid signup ARR in the prior period
  target: 
  org: Product
  public: false
  is_key: true
  health:
    level: 2
    reasons:
    - See notes in Key Meeting slides
  urls:
  - https://app.periscopedata.com/app/gitlab/527913/Product-KPIs?widget=7421392&udv=0
- name: Net Retention
  base_path: "/handbook/product/performance-indicators/"
  parent: "/handbook/sales/performance-indicators/#net-retention"
  definition: Owner of this KPI is the <a href="https://about.gitlab.com/direction/expansion/">Growth
    - Expansion</a> group.
  target: Our Target net retention rate is 180%.
  org: Product
  public: false
  is_key: true
  health:
    level: 2
    reasons:
    - See notes in Key Meeting slides
  urls:
  - https://app.periscopedata.com/app/gitlab/527913/Product-KPIs?widget=7104870&udv=0
- name: Gross Retention
  base_path: "/handbook/product/performance-indicators/"
  parent: "/handbook/sales/performance-indicators/#gross-retention"
  definition: Owner of this KPI is the <a href="https://about.gitlab.com/direction/retention/">Growth
    - Retention</a> group.
  target: Our Target gross retention rate is 90%.
  org: Product
  public: false
  is_key: true
  health:
    level: 3
    reasons:
    - See notes in Key Meeting slides
  urls:
  - https://app.periscopedata.com/app/gitlab/527913/Product-KPIs?widget=7104891&udv=0
- name: Stages per User (SpU)
  base_path: "/handbook/product/performance-indicators/"
  definition: Stages per user is calculated by dividing <a href="https://about.gitlab.com/handbook/product/metrics/#stage-monthly-active-users-smau">Stage
    Monthly Active User (SMAU)</a>  by <a href="https://about.gitlab.com/handbook/product/metrics/#monthly-active-users-mau">Monthly
    Active Users (MAU)</a>. The Stages per User (SpU) KPI is meant to capture the
    number of DevOps stages the average user is using on a monthly basis. This metric
    is important, as each stage added <a href="https://about.gitlab.com/direction/#strategic-response">triples
    paid conversion</a>.  Owner of this KPI is the VP, Product Management.
  target: Our Target SpU is 1.8.
  org: Product
  public: true
  is_key: true
  health:
    level: 2
    reasons:
    - See notes in Key Meeting slides
  sisense_data:
    chart: 8490496
    dashboard: 527913
    embed: v2
- name: Category Maturity Achievement
  base_path: "/handbook/product/performance-indicators/"
  definition: Percentage of category maturity plan achieved per quarter. Owner of
    this KPI is the VP, Product Management.
  target: Our Target is 70%.
  org: Product
  public: true
  is_key: true
  health:
    level: 2
    reasons:
    - See notes in Key Meeting slides
- name: Paid Net Promoter Score (PNPS)
  base_path: "/handbook/product/performance-indicators/"
  definition: Abbreviated as the PNPS acronym, please do not refer to it as NPS to
    prevent confusion. Measured as the percentage of paid customer "promoters" minus
    the percentage of paid customer "detractors" from a <a href="https://en.wikipedia.org/wiki/Net_Promoter#targetText=Net%20Promoter%20or%20Net%20Promoter,be%20correlated%20with%20revenue%20growth">Net
    Promoter Score</a> survey.  Note that while other teams at GitLab use a <a href="https://about.gitlab.com/handbook/business-ops/data-team/metrics/#satisfaction">satisfaction
    score</a>, we have chosen to use PNPS in this case so it is easier to benchmark
    versus other like companies.  Also note that the score will likely reflect customer
    satisfaction beyond the product itself, as customers will grade us on the total
    customer experience, including support, documentation, billing, etc. Owner of
    this KPI is Product Operations.
  target: Our Target PNPS is 40.
  org: Product
  public: true
  is_key: true
  health:
    level: 2
    reasons:
    - See notes in Key Meeting slides
  urls:
  - https://app.periscopedata.com/app/gitlab/527913/Product-KPIs?widget=8521210&udv=0
- name: Active Hosts
  base_path: "/handbook/product/performance-indicators/"
  definition: The count of active <a href="https://about.gitlab.com/pricing/#self-managed">Self
    Hosts</a>, Core and Paid, plus GitLab.com. Owner of this KPI is VP, Product Management.
    This is measured by counting the number of unique GitLab instances that send us
    <a href="https://docs.gitlab.com/ee/user/admin_area/settings/usage_statistics.html">usage
    ping</a>. We know from a <a href="https://app.periscopedata.com/app/gitlab/545874/Customers-and-Users-with-Usage-Ping-Enabled?widget=7126513&udv=937077">previous
    analysis</a> that only ~30% of licensed instances send us usage ping at least
    once a month.
  target: 
  org: Product
  public: true
  is_key: true
  health:
    level: 3
    reasons:
    - See notes in Key Meeting slides
  sisense_data:
    chart: 7441303
    dashboard: 527913
    embed: v2
- name: New hire location factor
  base_path: "/handbook/product/performance-indicators/"
  parent: "/handbook/people-group/performance-indicators/"
  target: Our Target is 0.72.
  org: Product
  public: true
  is_key: true
  health:
    level: 2
    reasons:
    - See notes in Key Meeting slides
  sisense_data:
    chart: 6838981
    dashboard: 527913
    embed: v2
- name: Refunds processed as % of orders
  base_path: "/handbook/product/performance-indicators/"
  definition: For a month take the number of refunds and divide that by the total
    amount of orders (including self-managed and sales initiated orders). More details
    <a href="https://about.gitlab.com/handbook/support/workflows/verify_subscription_plan.html#refunds-processed-as--of-orders">available
    here</a>
  target: 
  org: Product
  public: true
  is_key: true
  health:
    level: 2
    reasons:
    - See notes in Key Meeting slides
  sisense_data:
    chart: 7720228
    dashboard: 527913
    embed: v2
  urls:
  - https://app.periscopedata.com/app/gitlab/527913/Product-KPIs?widget=8131064&udv=0
- name: Acquisition impact
  base_path: "/handbook/product/performance-indicators"
  definition: Acquisition impact measures the number of <a href="https://about.gitlab.com/direction/maturity/">maturity
    levels</a> advanced, as a result of acquisitions. The target is 3 per year.
  target: 3 per year
  org: Product
  is_key: true
  public: true
  health:
    level: 3
    reasons:
    - TBD
- name: Stage Monthly Active Users (SMAU)
  base_path: "/handbook/product/performance-indicators/"
  definition: Stage Monthly Active Users is required for all product stages. SMAU
    is defined as the specified AMAU within a stage in a 28 day rolling period. Owner
    of this KPI is the VP, Product Management.
  target: 
  org: Product
  public: true
  is_key: false
  health:
    level: 2
    reasons:
    - See notes in Key Meeting slides
    - The SMAU candidate for each stage is selected by evaluation of Usage Ping data.
    - The SMAU for the Manage Stage (Owner - @jeremy) is in the evaluation phase.
    - The SMAU for the  Plan Stage (Owner - @justinfarris) is the number of unique
      authors who create issues (Issue.distinct_count_by(author_id)).
    - The SMAU for the  Create Stage (Owner - @ebrinkman) is the number of unique
      users who performed a repository write operation.
    - The SMAU for the  Verify Stage (Owner - @jyavorska) is the number of unique
      users who trigger ci_pipelines (Ci.pipeline.distinct_count_by(user_id)).
    - The SMAU for the  Package Stage (Owner - TBD) is in the evaluation phase.
    - The SMAU for the  Secure Stage (Owner - @david) is the number of unique users
      who have used one or more of the Secure scanners. This is tracked with the `user_unique_users_all_secure_scanners`
      statistic in usage ping. [More details on AMAU](/handbook/product/performance-indicators/#action-monthly-active-users-amau)
    - The SMAU for the  Release Stage (Owner - @jmeshell) is the number of unique
      users who trigger deployments (Deployment.distinct_count_by(user_id)).
    - The SMAU for the  Configure Stage (Owner - @nagyv-gitlab) will be a proxy metric
      of all the active project users for any projects with a cluster attached. [More
      info](https://gitlab.com/gitlab-org/growth/product/-/issues/1526)
    - The SMAU for the  Monitor Stage (Owner - @kbychu) will be a proxy metric of
      all the active project users for any projects with Prometheus enabled. [More
      info](https://gitlab.com/gitlab-org/growth/product/-/issues/1526)
    - The SMAU for the Defend Stage (Owner - TBD) is in the evaluation phase.
  sisense_data:
    chart: 8131064
    dashboard: 527913
    embed: v2
- name: Paid Stage Monthly Active Users (Paid SMAU)
  base_path: "/handbook/product/performance-indicators/"
  definition: Paid Stage Monthly Active Users is required for all product stages.
    Paid SMAU is defined as the count of SMAU (Stage Monthly Active Users) that roll
    up to paid accounts in a 28 day rolling period.  The license information used
    to generate this metric is being validated, and the currently displayed data could
    be incorrect by up to 20%.  Numbers displayed below for self-managed are for instances
    with usage ping (<a href="https://app.periscopedata.com/app/gitlab/634200/Usage-Ping-SMAU-Dashboard?widget=8462059&udv=0">enabled</a>)
    . Owner of this KPI is the VP, Product Management.
  target: TBD
  org: Product
  public: true
  is_key: false
  health:
    level: 0
    reasons:
    - TBD
  sisense_data:
    chart: 8887064
    dashboard: 634200
    embed: v2
- name: Stage Monthly Active Clusters (SMAC)
  base_path: "/handbook/product/performance-indicators/"
  definition: In categories where there is not a directly applicable SMAU metric as
    the category is focused on protecting our customer's customer, other metrics need
    to be reviewed and applied.  An example of a group where this applies is [Defend's
    Container Security group](/handbook/product/product-categories/#container-security-group)
    where their categories are designed to protect our customers and by extension
    their customers who are actively interacting with our customer's production /
    operations environment.  Metrics such as stage monthly active clusters (SMAC)
    can be used in place of SMAU until it is possible to achieve an accurate metric
    of true SMAU which may include measurements of monthly active sessions into the
    customer's clusters.
  target: TBD
  org: Product
  public: true
  is_key: false
  health:
    level: 0
    reasons:
    - TBD
- name: Group Monthly Active Users (GMAU)
  base_path: "/handbook/product/performance-indicators/"
  definition: GMAU is defined as the count of unique users who performed a particular
    action, or set of actions, within a group in a 28 day rolling period. Each R&D
    group is expected to have a primary performance indicator they are using to gauge
    the impact of their group on customer outcomes.  Where possible, groups should
    use Group Monthly Active Users (GMAU) or Group Monthly Active Paid Users (GMAPU)
    as their primary performance indicator.  GMAU should be used when the group is
    early in its maturity and its primary goal is new user adoption.  Paid GMAU should
    be the primary performance indicator when groups are more mature and have proven
    an ability to drive incremental IACV.  We prefer measuring monthly active usage
    over other potential measures like total actions taken (eg total dashboard views),
    as we believe that measuring active usage is a better measure of true user engagement
    with GitLab's product. Since R&D groups often contain more than one category,
    picking one category to base the action, or set of actions on, is a recommended
    simplification as we do not have the data bandwidth to support measuring every
    category's usage at this time.
  target: TBD
  org: Product
  public: true
  is_key: false
  health:
    level: 0
    reasons:
    - The GMAU candidates can be found on each [Product Section Performance Indicator
      Page](https://about.gitlab.com/handbook/product/performance-indicators/#other-pi-pages)
    - The [Dev Section Performance Indicator Page](https://about.gitlab.com/handbook/product/dev-section-performance-indicators/)
    - The [Ops Section Performance Indicator Page](https://about.gitlab.com/handbook/product/ops-section-performance-indicators/)
    - The [Secure & Defend Section Performance Indicator Page](https://about.gitlab.com/handbook/product/secure-and-defend-section-performance-indicators/)
    - The [Enablement Section Performance Indicator Page](https://about.gitlab.com/handbook/product/enablement-section-performance-indicators/)
- name: Paid Group Monthly Active Users (Paid GMAU)
  base_path: "/handbook/product/performance-indicators/"
  definition: Paid GMAU is defined as the count of GMAU (Group Monthly Active Users)
    that roll up to paid accounts in a 28 day rolling period. When Paid GMAU is the
    focus area for a group, they should measure actions tied to`Up-tiered features`,
    which are features that are unavailable in the free tier, as this will ensure
    we are tracking usage that has a clear tie to incremental IACV.
  target: TBD
  org: Product
  public: true
  is_key: false
  health:
    level: 0
    reasons:
    - The Paid GMAU candidates can be found on each [Product Section Performance Indicator
      Page](https://about.gitlab.com/handbook/product/performance-indicators/#other-pi-pages)
    - The [Dev Section Performance Indicator Page](https://about.gitlab.com/handbook/product/dev-section-performance-indicators/)
    - The [Ops Section Performance Indicator Page](https://about.gitlab.com/handbook/product/ops-section-performance-indicators/)
    - The [Secure & Defend Section Performance Indicator Page](https://about.gitlab.com/handbook/product/secure-and-defend-section-performance-indicators/)
    - The [Enablement Section Performance Indicator Page](https://about.gitlab.com/handbook/product/enablement-section-performance-indicators/)
- name: Action Monthly Active Users (AMAU)
  base_path: "/handbook/product/performance-indicators/"
  definition: An action describes an interaction the user has within a stage. The
    actions that need to be tracked have to be pre-defined. AMAU is defined as the
    number of unique users for a specific action in a 28 day rolling period. AMAU
    helps to measure the success of features. Note - There are metrics in usage ping
    under usage activity by stage that are not user actions and these should not be
    used for AMAU. Examples include Groups - `GroupMember.distinct_count_by(user_id)`,
    which is the number of distinct users added to groups, regardless of activity,
    ldap_group_links - `count(LdapGroupLink)`, which is a count of ldap group links
    and not a user initiated action, and projects_with_packages - `Project.with_packages.distinct_count_by(creator_id)`,
    which is a setting not an action.
  target: TBD
  org: Product
  public: true
  is_key: false
  health:
    level: 0
    reasons:
    - Dashboard [Issue](https://gitlab.com/gitlab-data/analytics/issues/1466)
- name: Active Churned User
  base_path: "/handbook/product/performance-indicators/"
  definition: A GitLab.com user, who is not a MAU in month T, but was a MAU in month
    T-1.
  target: TBD
  org: Product
  public: true
  is_key: false
  health:
    level: 0
    reasons:
    - TBD
  urls:
  - https://app.periscopedata.com/app/gitlab/461561/GitLab.com-Health
- name: Active Retained User
  base_path: "/handbook/product/performance-indicators/"
  definition: A GitLab.com user, who is a MAU both in months T and T-1.
  target: TBD
  org: Product
  public: true
  is_key: false
  health:
    level: 0
    reasons:
    - TBD
  urls:
  - https://app.periscopedata.com/app/gitlab/461561/GitLab.com-Health
- name: Paid User
  base_path: "/handbook/product/performance-indicators/"
  definition: A GitLab.com <a href="https://app.periscopedata.com/app/gitlab/500504/Licensed-Users-by-Rate-Plan-Name">Licensed
    User</a>.
  target: TBD
  org: Product
  public: true
  is_key: false
  health:
    level: 0
    reasons:
    - TBD
  urls:
  - https://gitlab.com/gitlab-data/analytics/issues/1445
- name: Paid MAU
  base_path: "/handbook/product/performance-indicators/"
  definition: A <a href="https://about.gitlab.com/handbook/finance/gitlabcom-metrics/index.html#paid-user">paid</a>
    <a href="/handbook/finance/gitlabcom-metrics/index.html#monthly-active-user-mau">MAU</a>.
  target: TBD
  org: Product
  public: true
  is_key: false
  health:
    level: 0
    reasons:
    - TBD
  urls:
  - https://gitlab.com/gitlab-data/analytics/issues/1446
- name: Monthly Active Namespace (MAN)
  base_path: "/handbook/product/performance-indicators/"
  definition: A GitLab <a href="https://docs.gitlab.com/ee/user/group/">Group</a>,
    which contains at least 1 <a href="https://docs.gitlab.com/ee/user/project/">project</a>
    since inception and has at least 1 <a href="https://docs.gitlab.com/ee/api/events.html)">Event</a>
    in a calendar month.
  target: TBD
  org: Product
  public: true
  is_key: false
  health:
    level: 0
    reasons:
    - TBD
  urls:
  - https://gitlab.com/gitlab-data/analytics/issues/1448
- name: Active Churned Group
  base_path: "/handbook/product/performance-indicators/"
  definition: A GitLab.com group, which is not a MAG in month T, but was a MAG in
    month T-1.
  target: TBD
  org: Product
  public: true
  is_key: false
  health:
    level: 0
    reasons:
    - TBD
  urls:
  - https://gitlab.com/gitlab-data/analytics/issues/1449
- name: Active Retained Namespace
  base_path: "/handbook/product/performance-indicators/"
  definition: A GitLab.com namespace both in months T and T-1.
  target: TBD
  org: Product
  public: true
  is_key: false
  health:
    level: 0
    reasons:
    - TBD
  urls:
  - https://gitlab.com/gitlab-data/analytics/issues/1450
- name: New Namespace
  base_path: "/handbook/product/performance-indicators/"
  definition: A GitLab.com namespace created in the last 30 days.
  target: TBD
  org: Product
  public: true
  is_key: false
  health:
    level: 0
    reasons:
    - TBD
  urls:
  - https://gitlab.com/gitlab-data/analytics/issues/1451
- name: Paid Namespace
  base_path: "/handbook/product/performance-indicators/"
  definition: A GitLab.com namespace, which is part of a paid plan, i.e. Bronze, Silver
    or Gold. <a href="https://about.gitlab.com/blog/2018/06/05/gitlab-ultimate-and-gold-free-for-education-and-open-source/">Free
    licenses for Ultimate and Gold</a> are currently included.
  target: TBD
  org: Product
  public: true
  is_key: false
  health:
    level: 0
    reasons:
    - TBD
  urls:
  - https://gitlab.com/gitlab-data/analytics/issues/1452
- name: Paid MAG
  base_path: "/handbook/product/performance-indicators/"
  definition: A <a href="https://about.gitlab.com/handbook/finance/gitlabcom-metrics/index.html#paid-group">paid</a>
    <a href="https://about.gitlab.com/handbook/finance/gitlabcom-metrics/index.html#monthly-active-group-mag">MAG</a>
  target: TBD
  org: Product
  public: true
  is_key: false
  health:
    level: 0
    reasons:
    - TBD
  urls:
  - https://gitlab.com/gitlab-data/analytics/issues/1453
- name: Product Tier Upgrade/Downgrade Rate
  base_path: "/handbook/product/performance-indicators/"
  definition: This is the conversion rate of customers moving from tier to tier
  target: TBD
  org: Product
  public: true
  is_key: false
  health:
    level: 0
    reasons:
    - TBD
  urls:
  - https://gitlab.com/gitlab-data/analytics/issues/3084
- name: Lost instances
  base_path: "/handbook/product/performance-indicators/"
  definition: A lost instance of self-managed GitLab didn't send a usage ping in the
    given month but it was active in the previous month
  target: TBD
  org: Product
  public: true
  is_key: false
  health:
    level: 0
    reasons:
    - TBD
  urls:
  - https://gitlab.com/gitlab-data/analytics/issues/1461
- name: User Return Rate
  base_path: "/handbook/product/performance-indicators/"
  definition: Percent of users or groups that are still active between the current
    month and the prior month.
  target: TBD
  org: Product
  public: true
  is_key: false
  health:
    level: 0
    reasons:
    - TBD
  urls:
  - https://gitlab.com/gitlab-data/analytics/issues/1433
- name: Churn
  base_path: "/handbook/product/performance-indicators/"
  definition: The opposite of User Return Rate. The percentage of users or groups
    that are no longer active in the current month, but were active in the prior month.
  target: TBD
  org: Product
  public: true
  is_key: false
  health:
    level: 0
    reasons:
    - TBD
- name: Stage Monthly Active Namespaces (SMAN)
  base_path: "/handbook/product/performance-indicators/"
  definition: Stage Monthly Active Namespaces is a KPI that is required for all product
    stages. SMAN is defined as the highest AMAN within a stage in a 28 day rolling
    period.
  target: TBD
  org: Product
  public: true
  is_key: false
  health:
    level: 0
    reasons:
    - TBD
- name: Action Monthly Active Namespaces (AMAN)
  base_path: "/handbook/product/performance-indicators/"
  definition: AMAN is defined as the number of unique namespaces in which that action
    was performed in a 28 day rolling period.
  target: TBD
  org: Product
  public: true
  is_key: false
  health:
    level: 0
    reasons:
    - TBD
- name: Acquisition success
  base_path: "/handbook/product/performance-indicators/"
  definition: Acquisition success measures how successful GitLab at integrating the
    new teams and technologies acquired. An acquisition is a success if it ships 70%
    of the acquired company's product functionality as part of GitLab within three
    months after acquisition. The acquisition success rate is the percentage of acquired
    companies that were successful. The target is 70% success rate.
  target: TBD
  org: Product
  public: true
  is_key: false
  health:
    level: 0
    reasons:
    - TBD
  urls:
  - 
- name: Sourced pipeline
  base_path: "/handbook/product/performance-indicators/"
  definition: The sourced pipeline includes companies operating in a DevOps segment
    relevant for GitLab. Companies have developed or are working on product functionality
    which is either a new category on our roadmap or at a more advanced maturity than
    our current categories. Target is ongoing monitoring of at least 1000 companies.
  target: TBD
  org: Product
  public: true
  is_key: false
  health:
    level: 0
    reasons:
    - TBD
