---
layout: markdown_page
title: "Product Direction - Monorepos"
description: "GitLab want to improve the monorepo experience. Learn more here!"
canonical_path: "/direction/monorepos/"
---

- TOC
{:toc}

Last reviewed: 2020-03-03

## Overview
Monolithic repositories, more commonly known as monorepos, are a software development approach where code for many projects is stored in a single repository.

Monorepos provide a number of advantages such as reduced complexity, code reuse, easier collaboration amongst teams, and streamlined dependency management; however, there are also several drawbacks such as scalability and performance concerns, large amounts of storage required, and loss of semantic versioning per service.

We want to improve the monorepo experience - whether it has multiple services that all get deployed together, or if it's a mobile app in a single repo that needs to be built completely differently for iOS and Android. We've made a number of great improvements here, such as parent-child pipelines, enhancing the performance of Git calls, as well as supporting Git LFS and partial clone.

The primary stages that will be supporting our development of a better monorepo experience are: 
- [Create](/direction/dev/#create-1)
- [Verify](/direction/verify)

## Existing monorepo experience inside of GitLab
GitLab already has a number of important features that help monorepos:

- [Git partial clone](https://docs.gitlab.com/ee/topics/git/partial_clone.html)
- [Git LFS](https://docs.gitlab.com/ee/administration/lfs/manage_large_binaries_with_git_lfs.html#git-lfs)
- [Parent-child pipelines](https://docs.gitlab.com/ee/ci/parent_child_pipelines.html)

## What's next & why
The primary way for us to improve the monorepo experience is by focusing on the performance of large files inside of repositories and streamlining the CI pipeline for them. We're currently working on the following two initiatives to help with both of those features:

- [Git for enormous repositories](https://gitlab.com/groups/gitlab-org/-/epics/773)
- [Dynamic pipeline creation for artifact includes](https://gitlab.com/gitlab-org/gitlab/issues/35632)

## Longer term

There are a few epics and important issues you can check out to see where we're headed.

- [&812](https://gitlab.com/groups/gitlab-org/-/epics/812): Make CI lovable for monorepos
