---
layout: handbook-page-toc
title: Requesting procurement services
---

<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

## On this page
{:.no_toc}

- TOC
{:toc}


## Requesting Procurement Services

Start working with the Procurement team by opening a vendor contract approval issue based on the type of purchase below. Procurement will not approve the request if the request is incomplete and/or missing information.

Contact Procurement directly in Slack via #procurement if you have questions.

**Note: Before sharing details and/or confidential information regarding our business needs, please obtain a [Mutual Non-Disclosure Agreement](https://drive.google.com/a/gitlab.com/file/d/1hRAMBYrYcd9yG8FOItsfN0XYgdp32ajt/view?usp=sharing) from the potential vendor(s).**

### 1: Purchase Type: Software/SaaS

1. Open a Vendor Contract Request issue with [this template](https://gitlab.com/gitlab-com/Finance-Division/procurement-team/procurement/-/issues/new?issuable_template=software_vendor_contract_request) to begin the process.
1. Create this issue **BEFORE** agreeing to business terms and/or pricing. 
1. It is preferred we negotiate the best pricing up front to keep our ongoing costs to a minimum across our long-term relationships with vendors. We also continue to evaluate supplier pricing at the time of renewal to minimize our ongoing costs across our long-term relationships with vendors.
1. ***For Field Marketing***: Approval should be requested according to the [Field Marketing Approval Matrix](/handbook/finance/procurement/procurement-services/#field-marketing-approval-matrix).

A video tutorial of the issue creation process can be found [**HERE**](https://gitlab.zoom.us/rec/share/z-4pcYnpxDJIac-d70_hRaIwFYj8eaa8g3VN8vJbnkypGYJSQZwIUL6R3gGWbxjb?startTime=1587585669000)

### 2: Purchase Type: Professional Services and all other contract types

1. Open a Vendor Contract Request issue with [this template](https://gitlab.com/gitlab-com/Finance-Division/procurement-team/procurement/-/issues/new?issuable_template=general_vendor_contract_request) 
1. Create this issue **BEFORE** agreeing to business terms and/or pricing. 
1. This template can be used for addendums that either do or do not change pricing.
1. ***For Field Marketing***: Approval should be requested according to the [Field Marketing Approval Matrix](/handbook/finance/procurement/procurement-services/#field-marketing-approval-matrix).

A video tutorial of the issue creation process can be found [**HERE**](https://gitlab.zoom.us/rec/share/6-h-N5zR71tIS5Xdsn-Pf7NiD53Eeaa80XMZ-fUNxEfuCbA_5yfQNyOY4AUZsmwh?startTime=1584653128000)


### 3: Purchase Type: Field Marketing and Events withOUT Confidential Data

1. Open a Vendor Contract Marketing & Events Request [issue with this template](https://gitlab.com/gitlab-com/Finance-Division/procurement-team/procurement/-/issues/new?issuable_template=vendor_contracts_marketing_events)
1. Examples for this template type include marketing events, programs, sponsorships, catering, hotels, swag and services that do NOT involve the processing or sharing of data.
1. Due to the rapid nature of these types of requests, procurement will only negotiate if spend is greater then $100K and there is business justification and alignment to do so. 
1. If you will be sharing confidential data with the vendor, please use the template under Purchase Type #2 above.

A video tutorial of the field marketing and events issue creation process can be found [**HERE**](https://gitlab.zoom.us/rec/share/vdJ0d5jS00RJTtKVxRj5fKkfMqXPaaa80XMY-fIFzx7GHZWxe_p688iPeZ_qU85O?startTime=1584654449000)


## Field Marketing Approval Matrix
If you are in Field Marketing, please note the below approval matrix. Select the appropriate approvals based on your contract threshold and copy and paste into the approval section of the Software/SaaS template.

1. Contracts between $0 & $9,999 USD can be approved by individual DRI 
1. Contracts between $10,000 - $19,999 USD require approval checkoff from individual DRI and the Field Marketing Country Manager 
1. Contracts between $20,000 - $25,000 USD require approval checkoff from individual DRI, the Field Marketing Country Manager and the Global Director of Field Marketing
1. Contracts over $25,000 USD require approval checkoff from individual DRI, the Field Marketing Country Manager, the Global Director of Field Marketing, the Sr. Director of Revenue Marketing and the CMO (who should be tagged by the Sr. Director of Revenue Marketing once their approval has been signed off)
